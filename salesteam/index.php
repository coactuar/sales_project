<!doctype html>
<html lang="en">
  <head>
    <title></title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="./css/style.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
/* body {
  font-family: "Lato", sans-serif;
} */

.sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: white;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

h4{
    color:saddlebrown;
}

.sidenav a{
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 22px;
  color: darkred;
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  /* color: #f1f1f1; */
  color:rosybrown;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

#main {
  transition: margin-left .5s;
  padding: 16px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
</style>
</head>
  <body id="bg">
    <div >
        <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; open</span>
    </div> 
    <div id="mySidenav" class="sidenav">
       
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a href="https://coact.live/productvideos/links.php">PRODUCT LINKS</a>
        <a href="https://coact.live/productvideos/dashboard.php">PRODUCT VIDEOS</a>
    </div>
  <section class="portfolio section" id="main">
        <ul class="portfolio-cats">
            <li data-filter="*">All</li>
            <li data-filter="Pharma">Pharma</li>
            <li data-filter="Healthcare">Healthcare</li>
            <li data-filter="IT">IT</li>
            <li data-filter="FMCG">FMCG</li>
            <li data-filter="Banking">Banking</li>
            <li data-filter="Automobiles">Automobiles/manufacturing</li>
            <li data-filter="Event">Event </li>
            <li data-filter="Energy">Energy</li>
        </ul>
        <div class="portfolio-gallery">
            <div class="portfolio-item" data-filter="Pharma">
                <div class="item-inner">
                  <a href="https://coact.live/allergan/beyondiop/" target="_blank" rel="noopener noreferrer"><img src="images\pharma\allergan beyondiop.png" alt=""></a>  
                </div>
            </div>
            <div class="portfolio-item" data-filter="Pharma">
                    <div class="item-inner">
                    <a href="https://coact.live/abb-gbs/" target="_blank" rel="noopener noreferrer"><img src="images\pharma\abb-gbs.png" alt=""></a>
                    </div>
            </div>

            <div class="portfolio-item" data-filter="Pharma">
                <div class="item-inner">
                <a href="https://coact.live/ajanta/netalo/" target="_blank" rel="noopener noreferrer"><img src="images\pharma\ajanta netalo.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="Pharma">
                <div class="item-inner">
                <a href="https://coact.live/biocon/g2g/" target="_blank" rel="noopener noreferrer"> <img src="images\pharma\biocon g2g.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="Pharma">
                <div class="item-inner">
                <a href="https://coact.live/Immuneel/" target="_blank" rel="noopener noreferrer"><img src="images\pharma\immuneel.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="Pharma">
                <div class="item-inner">
                <a href="https://coact.live/pharma/" target="_blank" rel="noopener noreferrer"><img src="images\pharma\pharma.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="Healthcare">
                <div class="item-inner">
                <a href="https://coact.live/E&Y/" target="_blank" rel="noopener noreferrer">  <img src="images\healthcare\E&Y.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="Healthcare">
                <div class="item-inner">
                <a href="https://coact.live/essitymasterclass_demo/" target="_blank" rel="noopener noreferrer"> <img src="images\healthcare\essitymasterclass-demo.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="Healthcare">
                <div class="item-inner">
                <a href="https://coact.live/XII250621/" target="_blank" rel="noopener noreferrer"> <img src="images\healthcare\XII250621.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="IT">
                <div class="item-inner">
                <a href="https://coact.live/capgemini/virtualtour/" target="_blank" rel="noopener noreferrer"> <img src="images\IT\capgemini-virtualtour.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="IT">
                <div class="item-inner">
                <a href="https://coact.live/capgemini/360demo/" target="_blank" rel="noopener noreferrer"> <img src="images\IT\capgemini-360demo.png" alt=""></a>
                </div>
            </div>

             <div class="portfolio-item" data-filter="IT">
                <div class="item-inner">
                <a href="https://coact.live/siemenshealthineers/KickoffY22/index.php" target="_blank" rel="noopener noreferrer">   <img src="images\IT\siemenshealthineers-KickoffY22.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="FMCG">
                <div class="item-inner">
                <a href="https://coact.live/britannia/" target="_blank" rel="noopener noreferrer"> <img src="images\FMCG\britannia.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="FMCG">
                <div class="item-inner">
                <a href="https://coact.live/LOREAL/" target="_blank" rel="noopener noreferrer"> <img src="images\FMCG\LOREAL.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="FMCG">
                <div class="item-inner">
                <a href="https://coact.live/Michaelkors/" target="_blank" rel="noopener noreferrer"><img src="images\FMCG\Michaelkors.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="FMCG">
                <div class="item-inner">
                <a href="https://coact.live/philips/sieindiasubcontinent/lobby.php" target="_blank" rel="noopener noreferrer"> <img src="images\FMCG\philips-sieindiasubcontinent.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="FMCG">
                <div class="item-inner">
                    <img src="images\FMCG\titan-togetHR.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="Banking">
                <div class="item-inner">
                <a href="https://coact.live/axisbank/" target="_blank" rel="noopener noreferrer"><img src="images\Banking\axisbank.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="Banking">
                <div class="item-inner">
                <a href="https://coact.live/bfil/" target="_blank" rel="noopener noreferrer"> <img src="images\Banking\bfil.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="Banking">
                <div class="item-inner">
                <a href="https://coact.live/E&Y/" target="_blank" rel="noopener noreferrer"> <img src="images\Banking\E&Y.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="Banking">
                <div class="item-inner">
                <a href="https://coact.live/SBIgeneral/webcast.php" target="_blank" rel="noopener noreferrer"><img src="images\Banking\SBIgeneral.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="Automobiles">
                <div class="item-inner">
                <a href="https://coact.live/Skoda/volkswagen/" target="_blank" rel="noopener noreferrer"><img src="images\Automobiles\skoda-volkswagen.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="Automobiles">
                <div class="item-inner">
                <a href="https://coact.live/tatamotors/190220/" target="_blank" rel="noopener noreferrer"> <img src="images\Automobiles\tatamotors-190220.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="Event">
                <div class="item-inner">
                <a href="https://coact.live/mantraproperties/" target="_blank" rel="noopener noreferrer"> <img src="images\event\mantraproperties.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="Event">
                <div class="item-inner">
                <a href="https://coact.live/kingfisherpremiumplansforfans/king11punjab/" target="_blank" rel="noopener noreferrer">  <img src="images\event\kingfisherpremium-king11punjab.png" alt=""></a>
                </div>
            </div>

            <div class="portfolio-item" data-filter="Energy">
                <div class="item-inner">
                <a href="https://coactx.live/GIZ/AnnualNetworkingMeet/lobby.php" target="_blank" rel="noopener noreferrer"> <img src="images\energy\GIZ-AnnualNetworkingMeet.png" alt=""></a>
                </div>
            </div>
            
        
        </div>
    </section>
      
  
  <div id="background-wrap">
      <div class="bubble x1"></div>
      <div class="bubble x2"></div>
      <div class="bubble x3"></div>
      <div class="bubble x4"></div>
      <div class="bubble x5"></div>
      <div class="bubble x6"></div>
      <div class="bubble x7"></div>
      <div class="bubble x8"></div>
      <div class="bubble x9"></div>
      <div class="bubble x10"></div>
      <div class="bubble x11"></div>
      <div class="bubble x12"></div>
      <div class="bubble x13"></div>
      <div class="bubble x14"></div>
      <div class="bubble x15"></div>
    </div>
<script src="./js/index.js"></script>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>