<?php
require_once "../config.php";

$sql = "SELECT `question`,`asked_at` FROM `tbl_sessionquestions`";  
$setRec = mysqli_query($link, $sql); 
$columnHeader = '';  
$columnHeader = "#" . "\t" . "Question" . "\t" . "Asked At" . "\t";  
$setData = '';  
  $i = 1;
  while ($rec = mysqli_fetch_row($setRec)) {  
    $rowData = '"'.$i.'"' . "\t";  
    foreach ($rec as $value) {  
        $value = '"' . $value . '"' . "\t";  
        $rowData .= $value;  
    }  
    $setData .= trim($rowData) . "\n";  
    //echo $rowData.'<br>';
    $i = $i + 1;
}  

  
$file = 'Questions.xls';  
header("Content-Type: application/octet-stream");  
header("Content-Disposition: attachment; filename=".$file);  
header("Pragma: no-cache");  
header("Expires: 0");  


echo ucwords($columnHeader) . "\n" . $setData . "\n";  

?>