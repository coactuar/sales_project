-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 04, 2022 at 03:09 PM
-- Server version: 8.0.27-0ubuntu0.20.04.1
-- PHP Version: 7.1.33-42+ubuntu20.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `salesteam_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sessionquestions`
--

CREATE TABLE `tbl_sessionquestions` (
  `id` int NOT NULL,
  `quesid` varchar(255) NOT NULL,
  `sessionid` varchar(255) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `question` text NOT NULL,
  `asked_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int NOT NULL,
  `name` varchar(100) NOT NULL,
  `reg_date` datetime NOT NULL,
  `employ_id` varchar(200) NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int DEFAULT '0',
  `location` varchar(200) DEFAULT NULL,
  `eventname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `name`, `reg_date`, `employ_id`, `login_date`, `logout_date`, `logout_status`, `location`, `eventname`) VALUES
(1, 'priyanka', '2022-01-03 12:46:02', 'globalhum2021@123.co', '2022-01-03 12:46:02', '2022-01-03 12:46:05', 0, '1234', 'COACT');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_sessionquestions`
--
ALTER TABLE `tbl_sessionquestions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_sessionquestions`
--
ALTER TABLE `tbl_sessionquestions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
